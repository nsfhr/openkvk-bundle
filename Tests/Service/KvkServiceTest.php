<?php
namespace Ytech\Bundle\OpenKvkBundle\Tests;

use Ytech\Bundle\OpenKvkBundle\Service\KvkService;
use Symfony\Component\Serializer\Serializer;

class KvkServiceTest extends \PHPUnit_Framework_TestCase
{
    protected $object;
    protected $mockSerializer;
    protected $mockHttpClient;

    public function setUp()
    {
        $this->mockSerializer = $this->getMock('\Symfony\Component\Serializer\SerializerInterface');
        $this->mockHttpClient = $this->getMock('\Guzzle\Http\Client');

        $this->object = $this->getMock(
            'Ytech\Bundle\OpenKvkBundle\Service\KvkService',
            array('doClientRequest'),
            array($this->mockHttpClient, $this->mockSerializer)
        );
    }

    public function testGetQueryResultReturnsEmpty()
    {
        $mockSerializedData = '[{"RESULT":{"TYPES":["bigint"],"HEADER":["kvk"],"ROWS":[]}}]';
        $mockDeserialized = array(array(
            'RESULT' => array('TYPES' => 'bigint', "HEADER" => 'kvk', 'ROWS' => array()
            )));
        $mockQuery = 'SELECT * From NiceTest;';

        $this->object->expects($this->once())
            ->method('doClientRequest')
            ->with(KvkService::URL_QUERY, $mockQuery)
            ->will($this->returnValue($mockSerializedData));

        $this->mockSerializer->expects($this->once())
            ->method('deserialize')
            ->with($mockSerializedData, 'array', 'json')
            ->will($this->returnValue($mockDeserialized));

        $this->assertEmpty($this->object->getQueryResult($mockQuery));
    }

    public function testGetOneResultReturnsNull()
    {
        $mockSerializedData = '[{"RESULT":{"TYPES":["bigint"],"HEADER":["kvk"],"ROWS":[]}}]';
        $mockDeserialized = array(array(
            'RESULT' => array('TYPES' => 'bigint', "HEADER" => 'kvk', 'ROWS' => array()
            )));
        $mockQuery = 'SELECT * From NiceTest;';

        $this->object->expects($this->once())
            ->method('doClientRequest')
            ->with(KvkService::URL_QUERY, $mockQuery)
            ->will($this->returnValue($mockSerializedData));

        $this->mockSerializer->expects($this->once())
            ->method('deserialize')
            ->with($mockSerializedData, 'array', 'json')
            ->will($this->returnValue($mockDeserialized));

        $this->assertNull($this->object->getOneQueryResult($mockQuery));
    }

    public function testSearch()
    {
        $mockSerializedData = '["ok" => "test"]';
        $mockDeserialized = array("ok" => "test");
        $mockSearch = 'banana';

        $this->object->expects($this->once())
            ->method('doClientRequest')
            ->with(KvkService::URL_SEARCH, $mockSearch)
            ->will($this->returnValue($mockSerializedData));

        $this->mockSerializer->expects($this->once())
            ->method('deserialize')
            ->with($mockSerializedData, 'array', 'json')
            ->will($this->returnValue($mockDeserialized));

        $this->assertEquals($mockDeserialized, $this->object->search($mockSearch));
    }

}