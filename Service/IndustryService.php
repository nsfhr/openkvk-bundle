<?php
namespace Ytech\Bundle\OpenKvkBundle\Service;

/**
 * Service for querying industry (SBI) data
 *
 * @package Ytech\Bundle\OpenKvkBundle\Service
 */
class IndustryService extends AbstractService
{

    /**
     * Returns an array with all industries
     *
     * @param int $limit optional
     * @param int $offset optional
     * @return array
     */
    public function getAllIndustries($limit = 9999, $offset = 0)
    {
        $query = 'SELECT * FROM sbi_codes LIMIT {$limit} OFFSET {$offset};';
        return $this->getQueryResult($query);
    }

    /**
     * Returns an array of the level 1 industries (01, 02, etc)
     *
     * @return array
     */
    public function getLevel1Industries()
    {
        $query = 'SELECT * FROM sbi_codes WHERE NOT code LIKE \'%.%\' LIMIT 9999;';
        return $this->getQueryResult($query);
    }

    /**
     * Returns an array of the level 2 industries (01.01, 01.02 etc) for the given level 1 industry
     * @param $level1
     * @return array
     */
    public function getLevel2Industries($level1)
    {
        $query = "SELECT * FROM sbi_codes WHERE code LIKE '".$level1.".%' AND NOT code LIKE '%.%.%' LIMIT 9999;";
        return $this->getQueryResult($query);
    }

    /**
     * Returns all level 3 subindustries of the given level2 industry
     * @param $level2
     * @return array
     */
    public function getLevel3Industries($level2)
    {
        return $this->getSubindustries($level2);
    }

    /**
     * Returns all subindustries of the given industry
     * @param $industry
     * @return array
     */
    public function getSubindustries($industry)
    {
        $query = 'SELECT * FROM sbi_codes WHERE code LIKE \''.$industry.'.%\' LIMIT 9999;';
        return $this->getQueryResult($query);
    }

    /**
     * Returns an array with the industries set for the given kvk number
     *
     * @param $kvkNumber
     * @return array
     */
    public function getIndustriesForKvk($kvkNumber)
    {
        $query = sprintf(
            "SELECT sbi_codes.*
                FROM kvk_sbi, sbi_codes
                WHERE
                    kvk_sbi.kvk = '%d'
                    AND kvk_sbi.code = sbi_codes.code
                LIMIT 100;",
            $kvkNumber
        );
        return $this->getQueryResult($query);
    }
}