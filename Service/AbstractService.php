<?php
namespace Ytech\Bundle\OpenKvkBundle\Service;

use Symfony\Component\Serializer\Serializer;
use Guzzle\Http\Client;

/**
 * Base for creating new openkvk-based services
 *
 * @package Ytech\Bundle\OpenKvkBundle\Service
 */
abstract class AbstractService
{
    /**
     * The base url for SQL-like queries
     */
    const URL_QUERY = 'http://api.openkvk.nl/json/';

    /**
     * The base url for simple search
     */
    const URL_SEARCH = 'http://officieel.openkvk.nl/json/';

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @param $client
     * @param $serializer
     */
    public function __construct($client, $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    /**
     * Decodes the given string from json to arrays
     * @param $stringResult
     * @return array
     */
    protected function deserialize($stringResult)
    {
        return $this->serializer->deserialize($stringResult, 'array', 'json');
    }

    /**
     * Runs the given query (for json output) and returns the results in an array
     * @param $query
     * @return array
     */
    public function getQueryResult($query)
    {
        // normalize multiline queries
        $query = str_replace("\n", ' ', $query);

        try {
            $stringResult = $this->doClientRequest(self::URL_QUERY, $query);
            $arrayResult = $this->deserialize($stringResult);

            // convert into associative array format
            return $this->parseQueryResult($arrayResult);
        } catch (\Exception $e) {
            // fail silently
            return array();
        }
    }

    /**
     * Searches the simple daily api for a string
     *
     * That could be a kvk number, postcode or any basic information
     * however the results are limited to 10 and may not be paginated
     *
     * @param $search
     * @return array
     */
    public function search($search)
    {
        try {
            $stringResult = $this->doClientRequest(self::URL_SEARCH, $search);
            return  $this->deserialize($stringResult);
        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * Performs a simple request and returns the body as a string.
     *
     * @param $baseUrl
     * @param $path
     * @return string
     */
    protected function doClientRequest($baseUrl, $path)
    {
        $this->client->setBaseUrl($baseUrl);
        $response = $this->client->get($path)->send();
        // return a string version of the body
        return $response->getBody(true);
    }

    /**
     * Runs the query and returns one result or null if none is found
     * @param $query
     * @return array|null
     */
    public function getOneQueryResult($query)
    {
        $results = $this->getQueryResult($query);
        return count($results) > 0 ? array_pop($results) : null;
    }

    /**
     * Returns an associative array with keys the field names returned by
     * the openkvk query api
     *
     * @param $arrayResult
     * @return array
     */
    protected function parseQueryResult($arrayResult)
    {
        $result = $arrayResult[0]['RESULT'];
        $combineWithKeys = function($r) use ($result) {
            return array_combine($result['HEADER'], $r);
        };
        return array_map($combineWithKeys, $result['ROWS']);
    }
}