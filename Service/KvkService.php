<?php

namespace Ytech\Bundle\OpenKvkBundle\Service;

/**
 * Service for querying the kvk tables
 *
 * @package Ytech\Bundle\OpenKvkBundle\Service
 */
class KvkService extends AbstractService
{
    /**
     * Returns company info given its kvk number
     * @param integer $kvkNumber
     * @return array
     */
    public function getByKvk($kvkNumber)
    {
        // we search via 'kvks' as 'kvk' is zero padded
        $query = sprintf("SELECT * FROM kvk WHERE kvks = '%d' LIMIT 1;", $kvkNumber);
        return $this->getOneQueryResult($query);
    }

    /**
     * Returns company info given its vestiging number
     * @param integer $vestigingNumber
     * @return array
     */
    public function getByVestiging($vestigingNumber)
    {
        $query = sprintf("SELECT * FROM kvk WHERE vestiging = '%d' LIMIT 1;", $vestigingNumber);
        return $this->getOneQueryResult($query);
    }

    /**
     * Returns tradename information given a kvk number
     * @param $kvkNumber
     * @return array
     */
    public function getTradename($kvkNumber)
    {
        $query = sprintf("SELECT * FROM kvk_handelsnamen WHERE kvks = '%d' LIMIT 1;", $kvkNumber);
        return $this->getOneQueryResult($query);
    }

    /**
     * Returns bankruptcies, latest first
     *
     * @param int $limit optional
     * @param int $offset optional
     *
     * @return array
     */
    public function getBankruptcies($limit = 9999, $offset = 0)
    {
        $query = "SELECT * FROM faillissementen ORDER BY datum DESC LIMIT {$limit} OFFSET {$offset};";
        return $this->getQueryResult($query);
    }
}