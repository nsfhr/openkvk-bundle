# openkvk-bundle [![Build Status](https://travis-ci.org/YTech/openkvk-bundle.png?branch=master)](https://travis-ci.org/YTech/openkvk-bundle)


Symfony 2 bundle for fetching [OpenKvk](https://openkvk.nl/api.html) data

## Install

Run `composer require "ytech/openkvk-bundle":"dev-master"`.

### Dependencies

The bundle requires `guzzle/guzzle` to perform the http requests.

## Services
The bundle provides two public services accessible via the service container:

- `ytech_openkvk.service.kvk`
- `ytech_openkvk.service.industry`

### Common methods in both services

- `getQueryResult`: Runs an openkvk query and returns the results
- `getOneQueryResult`: Runs a query and returns one result
- `search`: Performs simple search via the 'officieel' api. Limited to 10 results but up-to-date

### ytech_openkvk.service.kvk

- `getByKvk`: kvk table data
- `getByVestiging`: kvk table data
- `getTradename`: kvk_handelsnamen table data
- `getBankruptcies`: faillismenten table data

### ytech_openkvk.service.industry

- `getAllIndustries`: fetches all industries (sbi data)
- `getLevel1Industries`: only returns the top level industries (01, 02, etc)
- `getLevel2Industries`: level 2 subindustries of a level 1 industry (01.01, 01.02, etc)
- `getLevel3Industries`: level 3 subindustries of a level 2 industry (01.01.01, 01.01.02, etc)
- `getIndustriesForKvk`: returns the industries for a given kvk number
